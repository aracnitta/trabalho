        //
//  ViewController.swift
//  trabalho
//
//  Created by COTEMIG on 05/02/44 AH.
//

import UIKit
    
        struct Contato {
            let nome: String
            let numero: String
            let email: String
            let endereço: String
        }
        
class ViewController: UIViewController, UITableViewDataSource {

    var listadeContatos: [Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listadeContatos.count
            }
            
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
    let contato = listadeContatos[indexPath.row]
    
    cell.nome.text = contato.nome
    cell.numero.text = contato.numero
    cell.email.text = contato.email
    cell.endereço.text = contato.endereço
    
    return cell
}

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
    super.viewDidLoad()
        
    tableView.dataSource = self
        
        listadeContatos.append(Contato(nome: "contato 1", numero: "31 92345-86896", email: "contato1@email.com", endereço: "Rua maneira"))
        listadeContatos.append(Contato(nome: "contato 2", numero: "31 92345-6751", email: "contato2@email.com", endereço: "Rua descolada"))
        listadeContatos.append(Contato(nome: "contato 3", numero: "31 92774-6751", email: "contato3@email.com", endereço: "Rua chata"))
    }
}

       
